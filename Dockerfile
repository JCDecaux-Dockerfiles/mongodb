FROM jcdecaux/rhel:7
MAINTAINER Nicolas Mallet <nicolas.mallet@jcdecaux.com>

RUN yum install supervisor mongodb-server -y \
  && yum clean all

RUN mkdir -p /var/lib/mongodb \
  && touch /var/lib/mongodb/.keep \
  && chown -R mongodb:mongodb /var/lib/mongodb

COPY mongodb.conf /etc/mongodb.conf
COPY supervisord.conf /etc/supervisord.conf

VOLUME ["/var/lib/mongodb"]

EXPOSE 27017

CMD ["/usr/bin/supervisord"]
